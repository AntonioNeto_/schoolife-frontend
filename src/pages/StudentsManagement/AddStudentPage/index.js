import React from 'react'
import { useNavigate } from 'react-router-dom'
import StudentForm from '../StudentForm'

import './style.css'

export default function AddStudentPage({ ...props }) {
  const navigate = useNavigate()

  const handleAddStudent = async data => {
    // TODO: persist student
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/meus-estudantes')}>← Voltar</p>
        <div className='header-title'>Adicionar Estudante</div>
      </div>

      <div className='edit-package-content'>
        <h2>Dados</h2>
        <StudentForm
          formSubmitFunction={data => handleAddStudent(data)}
          action={'add'}
        />
      </div>
    </div>
  )
}
