import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import DropdownCard from '../../../components/DropdownCard'

import './style.css'

export default function StudentsManagement() {
  const navigate = useNavigate()

  const [students, setStudents] = useState([])

  useEffect(() => {
    setStudents([{
      name: 'Estudante 1',
      cpf: '11111111111',
      email: 'estudante1@email.com',
      id: 1
    }])
  }, [])

  const editStudent = (student) => {
    navigate(`/editar-estudante`, { state: { student } })
  }

  const removeFunction = (id) => {
    // TODO: remover estudante
  }

  const renderStudents = () => {
    return students.map(student => {
      return (
        <DropdownCard key={student.id} title={student.name} editFunction={() => editStudent(student)} removeFunction={removeFunction}>
          <p>
            <strong>Nome: </strong> {student.name}
          </p>
          <p>
            <strong>CPF: </strong> {student.cpf}
          </p>
          <p>
            <strong>E-mail: </strong> {student.email}
          </p>
        </DropdownCard>
      )
    })
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/')}>← Voltar</p>
        <div className='header-title'>Meus Estudantes</div>
      </div>

      <div className='students-management-content'>
        <h2>Estudantes</h2>
        <div className='students-container'>
          {renderStudents()}
        </div>
        <button className='add-student-button' onClick={() => navigate('/adicionar-estudante')}>Adicionar Estudante</button>
      </div>
    </div>
  )
}