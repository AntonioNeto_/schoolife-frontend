import React, { useEffect, useState } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'

import './style.css'
import Card from '../../../../components/Card'
import Popup from '../../../../components/Popup'


const allActivities = [
  {
    id: 0,
    name: 'Atividade 0',
    description: 'Descricao atividade 0'
  },
  {
    id: 1,
    name: 'Atividade 1',
    description: 'Descricao atividade 1'
  },
  {
    id: 2,
    name: 'Atividade 2',
    description: 'Descricao atividade 2'
  },
  {
    id: 3,
    name: 'Atividade 3',
    description: 'Descricao atividade 3'
  },
  {
    id: 4,
    name: 'Atividade 4',
    description: 'Descricao atividade 4'
  },
  {
    id: 5,
    name: 'Atividade 5',
    description: 'Descricao atividade 5'
  },
]

export default function EditStudentActivitiesPage({ ...props }) {
  const location = useLocation()
  const navigate = useNavigate()

  const [activities, setActivities] = useState([])

  const [showAddActivityPopup, setShowAddActivityPopup] = useState(false)

  useEffect(() => {
    setActivities([
      {
        id: 1,
        name: 'Atividade 1',
        description: 'Descricao atividade 1'
      }
    ])
  }, [])

  const removeActivity = () => {
    //TODO
  }

  const renderActivities = () => {
    return activities.map(activity => {
      return (
        <Card key={activity.id} classes='activity-card'>
          <div className="card-title">
            <strong>Atividade:</strong> {activity.name}
          </div>
          <div className="card-content">
            <p><strong>Descrição: </strong>{activity.description}</p>
            <button onClick={removeActivity}>Remover</button>
          </div>
        </Card>
      )
    })
  }

  const [selectedOption, setSelectedOption] = useState('')

  const handleOptionChange = (event) => {
    setSelectedOption(parseInt(event.target.value))
  }

  const renderAllActivities = () => {
    return allActivities
    .filter(activity => !activities.some(a => a.id === activity.id))
    .map(activity => {
      return (
        <Card key={activity.id} classes='subject-card'>
          <div className='card-title'>
            <input
              type='radio'
              id={`option-${activity.id}`}
              name='options'
              value={activity.id}
              checked={selectedOption === activity.id}
              onChange={handleOptionChange}
            />
            <label htmlFor={`option-${activity.id}`}> 
              <strong>Disciplina:</strong> {activity.name} <br/>
              <strong>Descrição:</strong> {activity.description}
            </label>
          </div>
        </Card>
      )
    })
  }

  const addActivity = () => {
    //TODO
    console.log(selectedOption)
    setShowAddActivityPopup(false)
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/meus-estudantes')}>← Voltar</p>
        <div className='header-title'>Editar Estudante</div>
      </div>

      <div className='edit-package-content'>
        <div className='inner-header'>
          <h2 onClick={() => navigate('/editar-estudante', { state: { student: location.state.student } })}>Dados</h2>
          <h2 className='selected'>Atividades</h2>
        </div>
        <div className="activities-container">
          {renderActivities()}
        </div>
      <button type='button' className='fixed-button' onClick={() => setShowAddActivityPopup(true)}>
        Adicionar Atividade
      </button>
      </div>
      {
        showAddActivityPopup && (
          <Popup closeFunction={() => setShowAddActivityPopup(false)}>
            <h2 className='popup-title'>Atividades</h2>

            <div className='subject-popup-content'>
              {renderAllActivities()}
            </div>
            <button className='subject-add-button' onClick={addActivity}>Adicionar Atividade</button>
          </Popup>
        )
      }
    </div>
  )
}
