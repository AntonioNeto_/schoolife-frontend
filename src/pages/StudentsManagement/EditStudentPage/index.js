import React, { useState, useEffect } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import StudentForm from '../StudentForm'

import './style.css'

export default function EditStudentPage({ ...props }) {
  const location = useLocation()
  const navigate = useNavigate()

  const [student, setStudent] = useState()

  useEffect(() => {
    if (location?.state?.student) {
      setStudent(location.state.student)
    } else {
      navigate('/')
    }
  }, [location?.state, navigate])

  const handleEditStudent = async data => {
    // TODO: persist student
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/meus-estudantes')}>← Voltar</p>
        <div className='header-title'>Editar Estudante</div>
      </div>

      <div className='edit-package-content'>
        <div className="inner-header">
          <h2 className='selected'>Dados</h2>
          <h2 onClick={() => navigate('/editar-estudante/atividades', { state: { student: location.state.student }})}>Atividades</h2>
        </div>
        <StudentForm
          formSubmitFunction={data => handleEditStudent(data)}
          defaultFormValues={student}
          action='edit'
        />
      </div>
    </div>
  )
}
