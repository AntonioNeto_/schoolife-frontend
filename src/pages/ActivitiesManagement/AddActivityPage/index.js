import React from 'react'
import { useNavigate } from 'react-router-dom'

import './style.css'
import ActivityForm from '../ActivityForm'

export default function AddActivityPage({ ...props }) {
  const navigate = useNavigate()

  const handleCreateActivity = async data => {
    // TODO: persist activity
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/atividades-extra-curriculares')}>← Voltar</p>
        <div className='header-title'>Criar Atividade</div>
      </div>

      <div className='edit-package-content'>
        <ActivityForm
          formSubmitFunction={data => handleCreateActivity(data)}
          action='add'
        />
      </div>
    </div>
  )
}
