import React, { useEffect, useMemo } from 'react'
import { object, string } from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form'

import './style.css'

const validationSchema = object({
  name: string().required('Nome é um campo obrigatório.'),
  description: string().required('Descrição é um campo obrigatório.'),
})

const defaultValues = {
  name: '',
  description: '',
}

export default function Activityform({
  formSubmitFunction,
  defaultFormValues = defaultValues,
  action,
  ...props
}) {
  const {
    register,
    reset,
    handleSubmit,
    formState: { errors }
  } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: {
      defaultValues: useMemo(() => {
        return defaultFormValues
      }, [defaultFormValues])
    }
  })

  useEffect(() => {
    if (action === 'add') return
    reset(defaultFormValues)
  }, [defaultFormValues, reset, action])

  const handleFormSubmit = data => {
    console.log(data)
    formSubmitFunction(data)
  }

  return (
    <form onSubmit={handleSubmit(handleFormSubmit)} className='student-form'>
      <div className='student-inputs'>
        <div className='student-input-container'>
          <label
            className='student-input-label'
            htmlFor='name-input'
          >
            Nome Atividade
          </label>
          <input
            type='text'
            placeholder='Nome da Atividade'
            {...register('name')}
          />
          <span
            className={
              errors?.name?.message ? 'field-error' : ''
            }
          >
            {errors?.name?.message}
          </span>
        </div>
        <div className='student-input-container'>
          <label className='student-input-label' htmlFor='description-input'>
            Descrição
          </label>
          <input
            placeholder='Descrição'
            type='text'
            {...register('description')}
          />
          <span
            className={
              errors?.description?.message ? 'field-error' : ''
            }
          >
            {errors?.description?.message}
          </span>
        </div>
      </div>

      <button
        classes={`primary-button ${action ? 'fixed-button' : ''}`}
        type='submit'
      >
        {
          action === 'edit'
            ? 'Salvar Atividade'
            : action === 'add' && 'Adicionar Atividade'
        }
      </button>
    </form>
  )
}
