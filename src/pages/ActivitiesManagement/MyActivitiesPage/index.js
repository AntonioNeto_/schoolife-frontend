import React, { useEffect, useState } from 'react'

import './style.css'
import DropdownCard from '../../../components/DropdownCard'
import { useNavigate } from 'react-router-dom'

export default function MyActivitiesPage() {
  const navigate = useNavigate()

  const [activities, setActivities] = useState([])

  useEffect(() => {
    setActivities([
      {
        id: 1,
        name: 'Atividade 1',
        description: 'Descrição atividade 1'
      },
      {
        id: 2,
        name: 'Atividade 2',
        description: 'Descrição atividade 2'
      },
      {
        id: 3,
        name: 'Atividade 3',
        description: 'Descrição atividade 3'
      },
    ])
  }, [])

  const removeFunction = (id) => {
    //TODO
  }

  const renderActivities = () => {
    return activities.map(activity => {
      return (
        <DropdownCard key={activity.id} title={activity.name} editFunction={() => navigate('/editar-atividade', { state: { activity } })} removeFunction={removeFunction}>
          <p>
            <strong>Atividade: </strong> {activity.name}
          </p>
          <p>
            <strong>Descrição: </strong> {activity.description}
          </p>
        </DropdownCard>
      )
    })
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/')}>← Voltar</p>
        <div className='header-title'>Atividades Extra Curricular</div>
      </div>

      <div className='activity-management-content'>
        <h2>Atividdades</h2>
        <div className='activities-container'>
          {renderActivities()}
        </div>
        <button className='add-student-button' onClick={() => navigate('/criar-atividade')}>Adicionar Atividade</button>
      </div>
    </div>
  )
}