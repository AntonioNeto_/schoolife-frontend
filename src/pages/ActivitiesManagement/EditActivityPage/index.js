import React, { useState, useEffect } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'

import './style.css'
import ActivityForm from '../ActivityForm'

export default function EditActivityPage({ ...props }) {
  const location = useLocation()
  const navigate = useNavigate()

  const [activity, setActivity] = useState()

  useEffect(() => {
    if (location?.state?.activity) {
      setActivity(location.state.activity)
    } else {
      navigate('/atividades-extra-curriculares')
    }
  }, [location?.state, navigate])

  const handleEditActivity = async data => {
    // TODO: persist activity
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/atividades-extra-curriculares')}>← Voltar</p>
        <div className='header-title'>Editar Atividade</div>
      </div>

      <div className='edit-package-content'>
        <ActivityForm
          formSubmitFunction={data => handleEditActivity(data)}
          defaultFormValues={activity}
          action='edit'
        />
      </div>
    </div>
  )
}
