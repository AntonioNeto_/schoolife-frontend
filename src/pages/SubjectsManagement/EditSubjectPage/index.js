import React, { useState, useEffect } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'

import './style.css'
import SubjectForm from '../SubjectForm'

export default function EditSubjectPage({ ...props }) {
  const location = useLocation()
  const navigate = useNavigate()

  const [subject, setSubject] = useState()

  useEffect(() => {
    if (location?.state?.subject) {
      setSubject(location.state.subject)
    } else {
      navigate('/disciplinas')
    }
  }, [location?.state, navigate])

  const handleEditSubject = async data => {
    // TODO: persist subject
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/disciplinas')}>← Voltar</p>
        <div className='header-title'>Editar Disciplina</div>
      </div>

      <div className='edit-package-content'>
        <SubjectForm
          formSubmitFunction={data => handleEditSubject(data)}
          defaultFormValues={subject}
          action='edit'
        />
      </div>
    </div>
  )
}
