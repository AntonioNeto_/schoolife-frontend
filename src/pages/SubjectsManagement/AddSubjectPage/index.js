import React from 'react'
import { useNavigate } from 'react-router-dom'

import './style.css'
import SubjectForm from '../SubjectForm'

export default function AddSubjectPage({ ...props }) {
  const navigate = useNavigate()

  const handleCreateSubject = async data => {
    // TODO: persist activity
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/disciplinas')}>← Voltar</p>
        <div className='header-title'>Criar Disciplina</div>
      </div>

      <div className='edit-package-content'>
        <SubjectForm
          formSubmitFunction={data => handleCreateSubject(data)}
          action='add'
        />
      </div>
    </div>
  )
}
