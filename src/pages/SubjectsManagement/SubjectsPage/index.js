import React, { useEffect, useState } from 'react'

import './style.css'
import DropdownCard from '../../../components/DropdownCard'
import { useNavigate } from 'react-router-dom'

export default function SubjectsPage() {
  const navigate = useNavigate()

  const [subjects, setSubjects] = useState([])

  useEffect(() => {
    setSubjects([
      {
        codigo: 1,
        name: 'Disciplina 1'
      },
      {
        codigo: 2,
        name: 'Disciplina 2'
      },
      {
        codigo: 3,
        name: 'Disciplina 3'
      },
    ])
  }, [])

  const removeFunction = (id) => {
    //TODO
  }

  const renderSubjects = () => {
    return subjects.map(subject => {
      return (
        <DropdownCard key={subject.codigo} title={subject.name} editFunction={() => navigate('/editar-disciplina', { state: { subject } })} removeFunction={removeFunction}>
          <p>
            <strong>Disciplina: </strong> {subject.name}
          </p>
        </DropdownCard>
      )
    })
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/')}>← Voltar</p>
        <div className='header-title'>Minhas Disciplinas</div>
      </div>

      <div className='activity-management-content'>
        <h2>Disciplinas</h2>
        <div className='activities-container'>
          {renderSubjects()}
        </div>
        <button className='add-student-button' onClick={() => navigate('/criar-disciplina')}>Adicionar Disciplina</button>
      </div>
    </div>
  )
}