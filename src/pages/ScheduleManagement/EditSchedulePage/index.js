import React, { useState, useEffect } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'

import './style.css'
import ScheduleForm from '../ScheduleForm'
import { formatHours, fullDateToStringISO } from '../../../util/dateFormatter'

export default function EditSchedulePage({ ...props }) {
  const location = useLocation()
  const navigate = useNavigate()

  const [schedule, setSchedule] = useState()

  useEffect(() => {
    if (location?.state?.schedule) {
      const { id, turma, sala, dataHoraInicio, dataHoraFim } =  location.state.schedule 
      const newSchedule = {
        id,
        turma,
        sala,
        date: fullDateToStringISO(dataHoraInicio),
        timeStart: formatHours(dataHoraInicio),
        timeEnd: formatHours(dataHoraFim)
      }
      setSchedule(newSchedule)
    } else {
      navigate('/horarios')
    }
  }, [location?.state, navigate])

  const handleEditSchedule = async data => {
    // TODO: persist schedule
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/horarios')}>← Voltar</p>
        <div className='header-title'>Editar Atividade</div>
      </div>

      <div className='edit-package-content'>
        <ScheduleForm
          formSubmitFunction={data => handleEditSchedule(data)}
          defaultFormValues={schedule}
          action='edit'
        />
      </div>
    </div>
  )
}
