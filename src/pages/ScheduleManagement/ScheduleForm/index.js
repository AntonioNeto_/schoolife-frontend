import React, { useEffect, useMemo } from 'react'
import { object, string, date } from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm, Controller } from 'react-hook-form'

import './style.css'

const validationSchema = object({
  turma: string().required('campo obrigatorio'),
  sala: string().required('campo obrigatorio'),
  id: string().nullable(),
  date: date().required('campo obrigatorio'),
  timeStart: string().required('obrigatorio'),
  timeEnd: string().required('campo obrigatorio')
})

const defaultValues = {
  turma: '',
  sala: '',
  date: new Date(),
  timeStart: '00:00',
  timeEnd: '00:00'
}

export default function Activityform({
  formSubmitFunction,
  defaultFormValues = defaultValues,
  action,
  ...props
}) {
  const {
    register,
    control,
    setValue,
    reset,
    handleSubmit,
    formState: { errors }
  } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: {
      defaultValues: useMemo(() => {
        return defaultFormValues
      }, [defaultFormValues])
    }
  })

  useEffect(() => {
    if (action === 'add') return
    reset(defaultFormValues)
  }, [defaultFormValues, reset, action])

  const handleFormSubmit = data => {
    console.log(data)
    formSubmitFunction(data)
  }

  useEffect(() => {
    if (defaultFormValues) {
      console.log('asd')
      setValue('date', defaultFormValues.date)
    }
  }, [defaultFormValues, setValue])

  return (
    <form onSubmit={handleSubmit(handleFormSubmit)} className='student-form'>
      <div className='student-inputs'>

        <div className='student-input-container'>
          <label
            className='student-input-label'
            htmlFor='turma-input'
          >
            Turma
          </label>
          <Controller
            name='turma'
            control={control}
            defaultValue=''
            render={({ field }) => (
              <select {...field}>
                <option value='' disabled>Selecione uma opção</option>
                {/* Adicionar turmas disponiceis */}
                <option value='1'>Turma 1</option>
                <option value='2'>Turma 2</option>
                <option value='3'>Turma 3</option>
              </select>
            )}
          />
          <span
            className={
              errors?.turma?.message ? 'field-error' : ''
            }
          >
            {errors?.turma?.message}
          </span>
        </div>

        <div className='student-input-container'>
          <label
            className='student-input-label'
            htmlFor='turma-input'
          >
            Sala
          </label>
          <Controller
            name='sala'
            control={control}
            defaultValue=''
            render={({ field }) => (
              <select {...field}>
                <option value='' disabled>Selecione uma opção</option>
                {/* Adicionar turmas disponiceis */}
                <option value='1'>Sala 1</option>
                <option value='2'>Sala 2</option>
                <option value='3'>Sala 3</option>
              </select>
            )}
          />
          <span
            className={
              errors?.sala?.message ? 'field-error' : ''
            }
          >
            {errors?.sala?.message}
          </span>
        </div>

        <div className='student-input-container'>
          <label
            className='student-input-label'
            htmlFor='date-input'
          >
            Data
          </label>
          <input
            type='date'
            {...register('date')}
          />
          <span
            className={
              errors?.date?.message ? 'field-error' : ''
            }
          >
            {errors?.date?.message}
          </span>
        </div>

        <div className='student-input-container'>
          <label
            className='student-input-label'
            htmlFor='timeStart-input'
          >
            Início
          </label>
          <input
            type='time'
            placeholder='Nome da Disciplina'
            {...register('timeStart')}
          />
          <span
            className={
              errors?.timeStart?.message ? 'field-error' : ''
            }
          >
            {errors?.timeStart?.message}
          </span>
        </div>
        
        <div className='student-input-container'>
          <label
            className='student-input-label'
            htmlFor='timeEnd-input'
          >
            Fim
          </label>
          <input
            type='time'
            placeholder='Nome da Disciplina'
            {...register('timeEnd')}
          />
          <span
            className={
              errors?.timeEnd?.message ? 'field-error' : ''
            }
          >
            {errors?.timeEnd?.message}
          </span>
        </div>
      </div>

      <button
        classes={`primary-button ${action ? 'fixed-button' : ''}`}
        type='submit'
      >
        {
          action === 'edit'
            ? 'Salvar Disciplina'
            : action === 'add' && 'Adicionar Disciplina'
        }
      </button>
    </form>
  )
}
