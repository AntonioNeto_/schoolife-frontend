import React from 'react'
import { useNavigate } from 'react-router-dom'

import './style.css'
import ScheduleForm from '../ScheduleForm'

export default function AddSchedulePage({ ...props }) {
  const navigate = useNavigate()

  const handleCreateSchedule = async data => {
    // TODO: persist activity
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/horarios')}>← Voltar</p>
        <div className='header-title'>Criar Horário</div>
      </div>

      <div className='edit-package-content'>
        <ScheduleForm
          formSubmitFunction={data => handleCreateSchedule(data)}
          action='add'
        />
      </div>
    </div>
  )
}
