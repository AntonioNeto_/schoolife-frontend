import React, { useEffect, useState } from 'react'

import './style.css'
import DropdownCard from '../../../components/DropdownCard'
import { useNavigate } from 'react-router-dom'
import { dateToString, formatHours } from '../../../util/dateFormatter'

export default function MySchedulesPage() {
  const navigate = useNavigate()

  const [schedules, setSchedules] = useState([])

  useEffect(() => {
    setSchedules([
      {
        id: 1,
        turma: '2',
        sala: '3',
        dataHoraInicio: new Date(2023, 3, 5, 10, 0, 0),
        dataHoraFim: new Date(2023, 3, 5, 12, 0, 0)
      },
      {
        id: 2,
        turma: '3',
        sala: '1',
        dataHoraInicio: new Date(2023, 3, 6, 10, 0, 0),
        dataHoraFim: new Date(2023, 3, 6, 10, 0, 0)
      },
      {
        id: 3,
        turma: '1',
        sala: '2',
        dataHoraInicio: new Date(2023, 3, 7, 10, 0, 0),
        dataHoraFim: new Date(2023, 3, 7, 10, 0, 0)
      },
    ])
  }, [])

  const removeFunction = (id) => {
    //TODO
  }

  const renderSchedules = () => {
    return schedules.map(schedule => {
      const title = `Turma ${schedule.turma} - ${dateToString(schedule.dataHoraInicio)} - ${formatHours(schedule.dataHoraInicio)} / ${formatHours(schedule.dataHoraFim)} `
      return (
        <DropdownCard key={schedule.id} title={title} editFunction={() => navigate('/editar-horario', { state: { schedule } })} removeFunction={removeFunction}>
          <p>
            <strong>TurmaId: </strong> {schedule.turma} <br/>
            <strong>SalaId: </strong> {schedule.sala} <br/>
            <strong>Data: </strong> {dateToString(schedule.dataHoraInicio)} <br/>
            <strong>Início: </strong> {formatHours(schedule.dataHoraInicio)} <br/>
            <strong>Fim: </strong> {formatHours(schedule.dataHoraFim)} <br/>
          </p>
        </DropdownCard>
      )
    })
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/')}>← Voltar</p>
        <div className='header-title'>Meus Horários</div>
      </div>

      <div className='activity-management-content'>
        <h2>Horários</h2>
        <div className='activities-container'>
          {renderSchedules()}
        </div>
        <button className='add-student-button' onClick={() => navigate('/criar-horario')}>Adicionar Horário</button>
      </div>
    </div>
  )
}