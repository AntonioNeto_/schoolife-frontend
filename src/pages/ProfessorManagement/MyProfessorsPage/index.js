import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import DropdownCard from '../../../components/DropdownCard'

import './style.css'

export default function ProfessorManagement() {
  const navigate = useNavigate()

  const [professors, setProfessors] = useState([])

  useEffect(() => {
    setProfessors([{
      name: 'Profesor 1',
      cpf: '11111111111',
      email: 'professor@email.com'
    }])
  }, [])

  const editProfessor = (professor) => {
    navigate(`/editar-professor`, { state: { professor } })
  }

  const removeFunction = (id) => {
    // TODO: remover professor
  }

  const renderProfessors = () => {
    return professors.map(professor => {
      return (
        <DropdownCard key={professor.cpf} title={professor.name} editFunction={() => editProfessor(professor)} removeFunction={removeFunction}>
          <p>
            <strong>Nome: </strong> {professor.name}
          </p>
          <p>
            <strong>CPF: </strong> {professor.cpf}
          </p>
          <p>
            <strong>E-mail: </strong> {professor.email}
          </p>
        </DropdownCard>
      )
    })
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/')}>← Voltar</p>
        <div className='header-title'>Professores</div>
      </div>

      <div className='students-management-content'>
        <h2>Professores</h2>
        <div className='students-container'>
          {renderProfessors()}
        </div>
        <button className='add-student-button' onClick={() => navigate('/adicionar-professor')}>Adicionar Professor</button>
      </div>
    </div>
  )
}