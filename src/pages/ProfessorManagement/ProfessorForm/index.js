import React, { useEffect, useMemo } from 'react'
import { object, string } from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form'

import './style.css'
import { normalizeCPF } from '../../../util/mask'

const validationSchema = object({
  name: string().required('Nome é um campo obrigatório.'),
  cpf: string().required('CPF é um campo obrigatório.'),
  email: string().required('Email é um campo obrigatório').email('Email mal formatado.')
})

const defaultValues = {
  name: '',
  cpf: '',
  email: ''
}

export default function ProfessorForm({
  formSubmitFunction,
  defaultFormValues = defaultValues,
  action,
  ...props
}) {
  const {
    register,
    watch,
    reset,
    setValue,
    handleSubmit,
    formState: { errors }
  } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: {
      defaultValues: useMemo(() => {
        return defaultFormValues
      }, [defaultFormValues])
    }
  })

  const cpf = watch('cpf')

  useEffect(() => {
    if(cpf) setValue('cpf', normalizeCPF(cpf))
  }, [cpf, setValue])

  useEffect(() => {
    if (action === 'add') return
    reset(defaultFormValues)
  }, [defaultFormValues, reset, action])

  const handleFormSubmit = data => {
    console.log(data)
    formSubmitFunction(data)
  }

  return (
    <form onSubmit={handleSubmit(handleFormSubmit)} className='student-form'>
      <div className='student-inputs'>
        <div className='student-input-container'>
          <label
            className='student-input-label'
            htmlFor='name-input'
          >
            Nome Profesor
          </label>
          <input
            type='text'
            placeholder='Nome'
            {...register('name')}
          />
          <span
            className={
              errors?.name?.message ? 'field-error' : ''
            }
          >
            {errors?.name?.message}
          </span>
        </div>
        <div className='student-input-container'>
          <label className='student-input-label' htmlFor='cpf-input'>
            CPF
          </label>
          <input
            placeholder='999.999.999-99'
            type='text'
            {...register('cpf')}
          />
          <span
            className={
              errors?.cpf?.message ? 'field-error' : ''
            }
          >
            {errors?.cpf?.message}
          </span>
        </div>
        <div className='student-input-container'>
          <label
            className='student-input-label'
            htmlFor='email'
          >
            E-mail
          </label>
          <input
            placeholder='email@email.com'
            type='text'
            {...register('email')}
          />

          <span
            className={
              errors?.email?.message
                ? 'field-error'
                : ''
            }
          >
            {errors?.email?.message}
          </span>
        </div>
      </div>

      <button
        classes={`primary-button ${action ? 'fixed-button' : ''}`}
        type='submit'
      >
        {
          action === 'edit'
            ? 'Salvar Professor'
            : action === 'add' && 'Adicionar Professor'
        }
      </button>
    </form>
  )
}
