import React, { useEffect, useState } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'

import './style.css'
import Card from '../../../../components/Card'
import Popup from '../../../../components/Popup'

//pegar disciplinas do backend
const allSubjects = [
  {
    codigo: 0,
    name: 'Disciplina 0'
  },
  {
    codigo: 1,
    name: 'Disciplina 1'
  },
  {
    codigo: 2,
    name: 'Disciplina 2'
  },
  {
    codigo: 3,
    name: 'Disciplina 3'
  },
  {
    codigo: 4,
    name: 'Disciplina 4'
  },
  {
    codigo: 5,
    name: 'Disciplina 5'
  },
  {
    codigo: 6,
    name: 'Disciplina 6'
  },
]

export default function EditProfessorSubjectsPage({ ...props }) {
  const location = useLocation()
  const navigate = useNavigate()

  const [subjects, setSubjects] = useState([])
  const [showAddSubjectPopup, setShowAddSubjectPopup] = useState(false)

  useEffect(() => {
    setSubjects([
      {
        codigo: 1,
        name: 'disciplina 1',
      }
    ])
  }, [])

  const removeSubject = () => {
    //TODO
  }

  const renderSubjects = () => {
    return subjects.map(subject => {
      return (
        <Card key={subject.codigo} classes='activity-card'>
          <div className='card-title'>
            <strong>Disciplina:</strong> {subject.name}
          </div>
          <div className='card-content'>
            <button onClick={removeSubject}>Remover</button>
          </div>
        </Card>
      )
    })
  }

  const [selectedOption, setSelectedOption] = useState('')

  const handleOptionChange = (event) => {
    setSelectedOption(parseInt(event.target.value))
  }

  const renderAllSubjects = () => {
    return allSubjects
    .filter(subject => !subjects.some(s => s.codigo === subject.codigo))
    .map(subject => {
      return (
        <Card key={subject.codigo} classes='subject-card'>
          <div className='card-title'>
            <input
              type='radio'
              id={`option-${subject.codigo}`}
              name='options'
              value={subject.codigo}
              checked={selectedOption === subject.codigo}
              onChange={handleOptionChange}
            />
            <label htmlFor={`option-${subject.codigo}`}> <strong>Disciplina:</strong> {subject.name}</label>
          </div>
        </Card>
      )
    })
  }

  const addSubject = () => {
    //TODO
    console.log(selectedOption)
    setShowAddSubjectPopup(false)
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/professores')}>← Voltar</p>
        <div className='header-title'>Editar Professor</div>
      </div>

      <div className='edit-package-content'>
        <div className='inner-header'>
          <h2 onClick={() => navigate('/editar-professor', { state: { professor: location.state.professor } })}>Dados</h2>
          <h2 className='selected'>Disciplinas</h2>
        </div>
        <div className='activities-container'>
          {renderSubjects()}
        </div>
        <button type='button' className='fixed-button' onClick={() => setShowAddSubjectPopup(true)}>
          Adicionar Disciplina
        </button>
      </div>
      {
        showAddSubjectPopup && (
          <Popup closeFunction={() => setShowAddSubjectPopup(false)}>
            <h2 className='popup-title'>Adicionar Disciplina</h2>

            <div className='subject-popup-content'>
              {renderAllSubjects()}
            </div>
            <button className='subject-add-button' onClick={addSubject}>Adicionar Disciplina</button>
          </Popup>
        )
      }
    </div>
  )
}
