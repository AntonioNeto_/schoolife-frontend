import React, { useState, useEffect } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import ProfessorForm from '../ProfessorForm'

import './style.css'

export default function EditProfessorPage({ ...props }) {
  const location = useLocation()
  const navigate = useNavigate()

  const [professor, setProfessor] = useState()

  useEffect(() => {
    if (location?.state?.professor) {
      setProfessor(location.state.professor)
    } else {
      navigate('/')
    }
  }, [location?.state, navigate])

  const handleEditProfessor = async data => {
    // TODO: persist professor
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/professores')}>← Voltar</p>
        <div className='header-title'>Editar Professor</div>
      </div>

      <div className='edit-package-content'>
        <div className="inner-header">
          <h2 className='selected'>Dados</h2>
          <h2 onClick={() => navigate('/editar-professor/disciplinas', { state: { professor: location.state.professor }})}>Disciplinas</h2>
        </div>
        <ProfessorForm
          formSubmitFunction={data => handleEditProfessor(data)}
          defaultFormValues={professor}
          action='edit'
        />
      </div>
    </div>
  )
}
