import React, { useEffect, useState } from 'react'

import './style.css'
import DropdownCard from '../../../components/DropdownCard'
import { useNavigate } from 'react-router-dom'
import { getTurmas } from '../../../util/api/apiCalls'

export default function SchoolClassesPage() {
  const navigate = useNavigate()

  const [schoolClasses, setSchoolClasses] = useState([])

  useEffect(() => {
    async function getAllTurmas() {
      try {
        const response = await getTurmas()
        setSchoolClasses(response.data)
      } catch (error) {
        console.error(error)
      }
    }

    getAllTurmas()
  }, [])

  const removeFunction = (id) => {
    //TODO
  }

  const renderStudents = (students) => {
    return (
      <div>
        <h3>Alunos:</h3>
        {students?.map((student, index) => {
          return (
            <p key={student?.id}>{index + 1}° - {student?.nome} </p>
          )
        })}
      </div>
    )
  }

  const renderClasses = () => {
    return schoolClasses?.map(schoolClass => {
      return (
        <DropdownCard
          key={schoolClass?.id}
          title={schoolClass?.nome}
          editFunction={() => navigate('/editar-turma', { state: { schoolClass } })}
          removeFunction={removeFunction} button='Adicionar Frequência'
          buttonAction={() => navigate('/frequencia', { state: { schoolClass } })}
        >
          <p>
            <strong>Turma: </strong> {schoolClass?.nome}
          </p>
          <p>
            <strong>Disciplina: </strong> {schoolClass?.disciplina?.nome}
          </p>
          {renderStudents(schoolClass?.alunos)}
        </DropdownCard>
      )
    })
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/')}>← Voltar</p>
        <div className='header-title'>Minhas Turmas</div>
      </div>

      <div className='activity-management-content'>
        <h2>Turmas</h2>
        <div className='activities-container'>
          {renderClasses()}
        </div>
        <button className='add-student-button' onClick={() => navigate('/criar-turma')}>Adicionar Turma</button>
      </div>
    </div>
  )
}