import React, { useState, useEffect } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'

import './style.css'
import SchoolClassesForm from '../SchoolClassesForm'

export default function EditSchoolClass({ ...props }) {
  const location = useLocation()
  const navigate = useNavigate()

  const [schoolClass, setSchoolClass] = useState()

  useEffect(() => {
    if (location?.state?.schoolClass) {
      setSchoolClass(location.state.schoolClass)
    } else {
      navigate('/turmas')
    }
  }, [location?.state, navigate])

  const handleEditSubject = async data => {
    // TODO: persist schoolClass
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/turmas')}>← Voltar</p>
        <div className='header-title'>Editar Turma</div>
      </div>

      <div className='edit-package-content'>
      <div className="inner-header">
          <h2 className='selected'>Dados</h2>
          <h2 onClick={() => navigate('/editar-turma/alunos', { state: { schoolClass: location.state.schoolClass }})}>Alunos</h2>
        </div>
        <SchoolClassesForm
          formSubmitFunction={data => handleEditSubject(data)}
          defaultFormValues={schoolClass}
          action='edit'
        />
      </div>
    </div>
  )
}
