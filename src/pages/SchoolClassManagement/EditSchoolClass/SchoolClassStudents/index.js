import React, { useEffect, useState } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'

import './style.css'
import Card from '../../../../components/Card'
import Popup from '../../../../components/Popup'
import { normalizeCPF } from '../../../../util/mask'

//pegar disciplinas do backend
const allStudents = [
  {
    name: 'Estudante 1',
    cpf: '11111111111',
    email: 'estudante1@email.com',
    id: 1
  },
  {
    name: 'Estudante 2',
    cpf: '22222222222',
    email: 'estudante2@email.com',
    id: 2
  },
  {
    name: 'Estudante 3',
    cpf: '33333333333',
    email: 'estudante3@email.com',
    id: 3
  },
  {
    name: 'Estudante 4',
    cpf: '44444444444',
    email: 'estudante4@email.com',
    id: 4
  },
  {
    name: 'Estudante 5',
    cpf: '55555555555',
    email: 'estudante5@email.com',
    id: 5
  },
  {
    name: 'Estudante 6',
    cpf: '66666666666',
    email: 'estudante6@email.com',
    id: 6
  },
  {
    name: 'Estudante 7',
    cpf: '77777777777',
    email: 'estudante7@email.com',
    id: 7
  },
  {
    name: 'Estudante 8',
    cpf: '88888888888',
    email: 'estudante8@email.com',
    id: 8
  },
  {
    name: 'Estudante 9',
    cpf: '99999999999',
    email: 'estudante9@email.com',
    id: 9
  }
]

export default function EditSchoolClassStudentsPage({ ...props }) {
  const location = useLocation()
  const navigate = useNavigate()

  const [students, setStudents] = useState([])
  const [showAddSubjectPopup, setShowAddSubjectPopup] = useState(false)

  useEffect(() => {
    if (location.state?.schoolClass) {
      setStudents(location.state.schoolClass.alunos)
    }
  }, [location.state])

  const removeStudent = () => {
    //TODO
  }

  const renderSubjects = () => {
    return students.map(subject => {
      return (
        <Card key={subject.id} classes='activity-card'>
          <div className='card-title'>
            <strong>Aluno:</strong> {subject.name} <br />
            <strong>CPF:</strong> {normalizeCPF(subject.cpf)}
          </div>
          <div className='card-content'>
            <button onClick={removeStudent}>Remover</button>
          </div>
        </Card>
      )
    })
  }

  const [selectedOption, setSelectedOption] = useState('')

  const handleOptionChange = (event) => {
    setSelectedOption(parseInt(event.target.value))
  }

  const renderAllStudents = () => {
    return allStudents
    .filter(student => !students.some(s => s.id === student.id))
    .map(student => {
      return (
        <Card key={student.id} classes='subject-card'>
          <div className='card-title'>
            <input
              type='radio'
              id={`option-${student.id}`}
              name='options'
              value={student.id}
              checked={selectedOption === student.id}
              onChange={handleOptionChange}
            />
            <label htmlFor={`option-${student.id}`}> <strong>Aluno:</strong> {student.name}</label>
          </div>
        </Card>
      )
    })
  }

  const addStudent = () => {
    //TODO
    console.log(selectedOption)
    setShowAddSubjectPopup(false)
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/turmas')}>← Voltar</p>
        <div className='header-title'>Editar Turma</div>
      </div>

      <div className='edit-package-content'>
        <div className='inner-header'>
          <h2 onClick={() => navigate('/editar-turma', { state: { schoolClass: location.state.schoolClass } })}>Dados</h2>
          <h2 className='selected'>Alunos</h2>
        </div>
        <div className='activities-container'>
          {renderSubjects()}
        </div>
        <button type='button' className='fixed-button' onClick={() => setShowAddSubjectPopup(true)}>
          Adicionar Aluno
        </button>
      </div>
      {
        showAddSubjectPopup && (
          <Popup closeFunction={() => setShowAddSubjectPopup(false)}>
            <h2 className='popup-title'>Adicionar Aluno</h2>

            <div className='subject-popup-content'>
              {renderAllStudents()}
            </div>
            <button className='subject-add-button' onClick={addStudent}>Adicionar Aluno</button>
          </Popup>
        )
      }
    </div>
  )
}
