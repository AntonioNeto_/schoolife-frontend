import React from 'react'

import './style.css'
import { useNavigate } from 'react-router-dom'

export default function MenuPage() {
  const navigate = useNavigate()

  return (
    <div className=''>
      <h1>Menu</h1>
      <ul>
        <li onClick={() => navigate('/meus-estudantes')}>Estudantes</li>
        <li onClick={() => navigate('/atividades-extra-curriculares')}>Atividades</li>
        <li onClick={() => navigate('/professores')}>Professores</li>
        <li onClick={() => navigate('/disciplinas')}>Disciplinas</li>
        <li onClick={() => navigate('/turmas')}>Turmas</li>
        <li onClick={() => navigate('/horarios')}>Horários</li>
      </ul>
    </div>
  )
}