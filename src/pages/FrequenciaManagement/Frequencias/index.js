import React, { useEffect, useState } from "react"
import { getFrequencias } from "../../../util/api/apiCalls"
import { dateToString } from "../../../util/dateFormatter"

export default function FrequenciaPage() {

  const [frequencias, setFrequencias] = useState([])

  useEffect(() => {
    const getAllFrequencias = async () => {
      try {
        const response = await getFrequencias()
        setFrequencias(response.data)
      } catch (error) {
        console.error(error)
      }
    }

    getAllFrequencias()
  }, [])

  const renderFrequencia = () => {
    return frequencias.map(frequencia => {
      return (
        <DropdownCard key={frequencia.id} title={dateToString(new Date(frequencia?.data?.replace('-', '/')))} editFunction={() => navigate('/editar-frequencia', { state: { frequencia } })} removeFunction={() => ''}>
          <p>
            <strong>Aluno: </strong> {frequencia.alunoMatricula} <br />
            <strong>Disciplina: </strong> {frequencia.disciplinaId} <br />
            <strong>Data: </strong> {dateToString(new Date(frequencia?.data?.replace('-', '/')))} <br />
            <strong>Presente: </strong> {frequencia.persenca ? 'Presente' : 'Faltou'} <br />
          </p>
        </DropdownCard>
      )
    })
  }


  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/')}>← Voltar</p>
        <div className='header-title'>Frequencias</div>
      </div>

      <div className='activity-management-content'>
        <h2>Disciplinas</h2>
        <div className='activities-container'>
          {renderFrequencia()}
        </div>
      </div>
    </div>
  )

}