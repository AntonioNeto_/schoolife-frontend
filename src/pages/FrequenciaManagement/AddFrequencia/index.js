import React, { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import Card from '../../../components/Card'
import { createFrequencia } from '../../../util/api/apiCalls'
import { fullDateToStringISO } from '../../../util/dateFormatter'

export default function AddFrequenciaPage({ ...props }) {
  const navigate = useNavigate()
  const location = useLocation()

  const [frequencia, setFrequencia] = useState([])

  useEffect(() => {
    if (location.state?.schoolClass) {
      const alunos = location.state.schoolClass?.alunos
      setFrequencia(alunos?.map(aluno => {
        return {
          nome: aluno?.nome,
          matricula: aluno?.matricula,
          presenca: false
        }
      }))
    }
  }, [location.state])

  const cadastraFrequencia = async (data) => {
    await createFrequencia(data)
  }

  const handleFormSubmit = () => {
    const { disciplina } = location.state.schoolClass
    const data = new Date()

    frequencia.forEach(aluno => {
      try {
        const { matricula, presenca } = aluno
        cadastraFrequencia({
          'alunoMatricula': matricula,
          'disciplinaId': disciplina.id,
          'data': fullDateToStringISO(data),
          presenca
        })
      } catch (error) {
        console.error(error)
      }
    })
  }

  const renderAlunos = () => {
    return (
      frequencia?.map((aluno, index) => {
        return (
          <Card key={aluno.matricula} classes='subject-card'>
            <div className='card-title'>
              <input
                type='radio'
                id={`option-${index}`}
                name='options'
                checked={aluno.presenca}
                onClick={() => setFrequencia(curr => {
                  const newFreq = [...curr]
                  newFreq[index] = { ...newFreq[index], presenca: !aluno.presenca }
                  return newFreq
                }
                )}
              />
              <label htmlFor={`option-${index}`}>
                <strong>Aluno:</strong> {aluno.nome} <br />
                <strong>Matricula:</strong> {aluno.matricula}
              </label>
            </div>
          </Card>
        )
      })
    )
  }

  return (
    <div className='page-container'>
      <div className='header-container'>
        <p className='header-return' onClick={() => navigate('/turmas')}>← Voltar</p>
        <div className='header-title'>Fazer Chamada</div>
      </div>

      <div className='edit-package-content'>
        <form onSubmit={handleFormSubmit} className='student-form'>
          {renderAlunos()}

          <button
            classes={'fixed-button'}
            type='submit'
          >
            Salvar Frequência
          </button>
        </form>
      </div>
    </div>
  )
}
