import API from './api'

const getFrequencias = () => API.get('/api/frequencias')

const createFrequencia = (data) => API.post('/api/frequencias', data)

const getFrequenciaById = (id) => API.get(`/api/frequencias/${id}`)

const updateFrequenciaById = (id, data) => API.put(`/api/frequencias/${id}`, data)

const deleteFrequenciaById = (id, data) => API.delete(`/api/frequencias/${id}`, data)

const getTurmas = () => API.get('/turmas')

export {
  getFrequencias,
  createFrequencia,
  getFrequenciaById,
  updateFrequenciaById,
  deleteFrequenciaById,
  getTurmas
}