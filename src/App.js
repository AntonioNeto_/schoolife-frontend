import './App.css'
import Router from './routes'

import './global/style.css'

function App() {
  return (
    <Router />
  )
}

export default App
