import { Navigate, Outlet } from 'react-router-dom'

const NotRequireAuth = () => {
  // TODO: pegar token gerado no login
  const token = false
  
  return (
    token
      ? <Navigate to='/' />
      : <Outlet />
  )
}
export default NotRequireAuth