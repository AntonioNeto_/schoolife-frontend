import React, { useState } from 'react'

import './style.css'
import Card from '../Card'

export default function DropdownCard({
  children,
  title,
  removeFunction,
  editFunction,
  readOnly = false,
  button,
  buttonAction,
  ...props
}) {
  const [showDetails, setShowDetails] = useState(false)

  const renderCard = () => {
    return (
      <div className={readOnly ? '' : `dropdown-card-content`}>
        {children}
        {!readOnly && (
          <div className='dropdown-card-buttons'>
            <button
              classes='action-button-tertiary action-button-small'
              onClick={() => {
                editFunction()
              }}
            >
              Editar
            </button>
            <button
              type='button'
              classes='alert-button'
              onClick={() => ''}
            >
              Remover
            </button>
            {
              button ?
              <button
                type='button'
                classes='alert-button'
                onClick={buttonAction}
              >
                {button}
              </button> : ''
            }
          </div>
        )}
      </div>
    )
  }

  return (
    <div className='dropdown-card-container'>
      <Card classes='dropdown-card'>
        <div
          className='dropdown-card-header'
          onClick={() => setShowDetails(current => !current)}
        >
          <h3>{title}</h3>
          <span>{showDetails ? '↑' : '↓'}</span>
        </div>
        {showDetails && renderCard()}
      </Card>
    </div>
  )
}
