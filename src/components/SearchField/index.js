import React, { useState, useEffect } from 'react'

import './style.css'

export default function SearchField ({
  onSearchSubmit = () => '',
  clearResults  = () => '',
  placeholder = '',
  ...props
}) {
  const [searchTerm, setSearchTerm] = useState('')
  useEffect(() => {
    if (searchTerm !== '') {
      onSearchSubmit(searchTerm)
    } else {
      clearResults()
    }
    // eslint-disable-next-line
  }, [searchTerm])

  return (
    <input
      placeholder={placeholder}
      type='text'
      name='search-bar'
      onChange={e => setSearchTerm(e.target.value)}
      value={searchTerm}
    >
    </input>
  )
}