import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import NotRequireAuth from './components/NotRequireAuth'
import RequireAuth from './components/RequireAuth'
import EditStudentPage from './pages/StudentsManagement/EditStudentPage'
import StudentsManagement from './pages/StudentsManagement/MyStudentsPage'
import AddStudentPage from './pages/StudentsManagement/AddStudentPage'
import EditStudentActivitiesPage from './pages/StudentsManagement/EditStudentPage/StudentActivitiesPage'
import MenuPage from './pages/MenuPage'
import MyActivitiesPage from './pages/ActivitiesManagement/MyActivitiesPage'
import EditActivityPage from './pages/ActivitiesManagement/EditActivityPage'
import AddActivityPage from './pages/ActivitiesManagement/AddActivityPage'
import ProfessorManagement from './pages/ProfessorManagement/MyProfessorsPage'
import EditProfessorPage from './pages/ProfessorManagement/EditProfessorPage'
import EditProfessorSubjectsPage from './pages/ProfessorManagement/EditProfessorPage/ProfessorSubjects'
import SubjectsPage from './pages/SubjectsManagement/SubjectsPage'
import EditSubjectPage from './pages/SubjectsManagement/EditSubjectPage'
import AddSubjectPage from './pages/SubjectsManagement/AddSubjectPage'
import SchoolClassesPage from './pages/SchoolClassManagement/SchoolClassesPage'
import EditSchoolClass from './pages/SchoolClassManagement/EditSchoolClass'
import EditSchoolClassStudentsPage from './pages/SchoolClassManagement/EditSchoolClass/SchoolClassStudents'
import MySchedulesPage from './pages/ScheduleManagement/MySchedules'
import AddSchedulePage from './pages/ScheduleManagement/AddSchedulePage'
import EditSchedulePage from './pages/ScheduleManagement/EditSchedulePage'
import AddFrequenciaPage from './pages/FrequenciaManagement/AddFrequencia'

export default function Router(props) {
    return (
        <BrowserRouter>
            <Routes>
                <Route element={<NotRequireAuth />}>
                    <Route path='/' element={<MenuPage />} />
                    <Route path='/meus-estudantes' element={<StudentsManagement />} />
                    <Route path='/editar-estudante' element={<EditStudentPage />} />
                    <Route path='/editar-estudante/atividades' element={<EditStudentActivitiesPage />} />
                    <Route path='/adicionar-estudante' element={<AddStudentPage />} />
                    <Route path='/atividades-extra-curriculares' element={<MyActivitiesPage />} />
                    <Route path='/editar-atividade' element={<EditActivityPage />} />
                    <Route path='/criar-atividade' element={<AddActivityPage />} />
                    <Route path='/professores' element={<ProfessorManagement />} />
                    <Route path='/editar-professor' element={<EditProfessorPage />} />
                    <Route path='/editar-professor/disciplinas' element={<EditProfessorSubjectsPage />} />
                    <Route path='/disciplinas' element={<SubjectsPage />} />
                    <Route path='/editar-disciplina' element={<EditSubjectPage />} />
                    <Route path='/criar-disciplina' element={<AddSubjectPage />} />
                    <Route path='/turmas' element={<SchoolClassesPage />} />
                    <Route path='/editar-turma' element={<EditSchoolClass />} />
                    <Route path='/editar-turma/alunos' element={<EditSchoolClassStudentsPage />} />
                    <Route path='/horarios' element={<MySchedulesPage />} />
                    <Route path='/criar-horario' element={<AddSchedulePage />} />
                    <Route path='/editar-horario' element={<EditSchedulePage />} />
                    <Route path='/frequencia' element={<AddFrequenciaPage />} />
                </Route>
                <Route element={<RequireAuth />}>

                </Route>
            </Routes>
        </BrowserRouter>
    )
}